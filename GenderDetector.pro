#-------------------------------------------------
#
# Project created by QtCreator 2016-01-04T21:13:29
#
#-------------------------------------------------

QT       -= gui

CONFIG += c++11
CONFIG += shared
TARGET = GenderDetector
TEMPLATE = lib

DEFINES += GENDERDETECTOR_LIBRARY

SOURCES += genderdetector.cpp

HEADERS += genderdetector.h\
        genderdetector_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
