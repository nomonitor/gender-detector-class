#include "genderdetector.h"


GenderDetector::GenderDetector(QString path="/test.txt")
{
    QFile * file = new QFile(QDir::currentPath()+path);
    QList<InputData> input = reader(file);
    trainer(input);
}
QList<GenderDetector::InputData> GenderDetector::reader(QFile * file)
{
    QList<InputData> ret_list;
    if(!file->open(QIODevice::ReadOnly)) {
        qDebug()<<file->errorString();
    }

    QTextStream in(file);
    in.setCodec(QTextCodec::codecForName("UTF-8"));

    while(!in.atEnd()) {
        ret_list.append(InputData(in.readLine().split(" ")));
    }
    file->close();
    return ret_list;
}

void GenderDetector::trainer(QList<InputData> input)
{
    for(auto &value : input)
    {
        gender.insert(value.gender,(gender[value.gender]==NULL)?1:++gender[value.gender]);
        QPair<QString,QString> keys_tmp = QPair<QString,QString>(value.gender,value.key);
        freq.insert(keys_tmp,(freq[keys_tmp]==NULL)?1:++freq[keys_tmp]);
    }

    for(auto e : freq.keys())
        freq.insert(e,freq.value(e)/gender.value(e.first));

    for(auto e : gender.keys())
        gender.insert(e,gender.value(e)/input.count());

}
bool GenderDetector::classify(QString name)
{
    qDebug()<<name;
    name = name.right(LETTERS_MAX);

    int curr_index = 0;
    int index_by_probability = 0;
    float tmp_probability=-100;
    for(auto e : gender.keys())
    {
        float curr_probability = -qLn(gender[e])+qLn(freq[QPair<QString,QString>(e,name)])/qLn(qPow(10,-7));
        if(curr_probability==INFINITY)
        {
            curr_index++;
            continue;
        }
        if(tmp_probability<curr_probability)
        {
            tmp_probability = curr_probability;
            index_by_probability = curr_index;
        }
        curr_index++;
    }
    curr_index=0;
    for(auto e : gender.keys())
    {
        if(curr_index==index_by_probability)
            qDebug()<<e;
        curr_index++;
    }

   return (index_by_probability==1)?true:false;
}


/*void GenderDetector::tester(QFile * file)
{
    if(!file->open(QIODevice::ReadOnly)) {
        qDebug()<<file->errorString();
    }

    QTextStream in(file);
    in.setCodec(QTextCodec::codecForName("UTF-8"));

    while(!in.atEnd()) {
        classify(in.readLine());
    }
    file->close();
}*/
