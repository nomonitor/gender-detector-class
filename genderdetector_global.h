#ifndef GENDERDETECTOR_GLOBAL_H
#define GENDERDETECTOR_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QDir>
#include <QFile>
#include <QtMath>
#include <QTextCodec>
#include <QTextStream>

#if defined(GENDERDETECTOR_LIBRARY)
#  define GENDERDETECTORSHARED_EXPORT Q_DECL_EXPORT
#else
#  define GENDERDETECTORSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // GENDERDETECTOR_GLOBAL_H
