#ifndef GENDERDETECTOR_H
#define GENDERDETECTOR_H

#include <QDebug>
#include "genderdetector_global.h"

class GENDERDETECTORSHARED_EXPORT GenderDetector
{
static const int LETTERS_MAX = 3;

public:
    GenderDetector(QString path);
    bool classify(QString name);

private:
    QMap<QString,float> gender;
    QMap<QPair<QString,QString>, float> freq;

    struct InputData{
        QString gender;
        QString key;
        InputData(QString _gender,QString _key):gender(_gender),key(_key)
        {}
        InputData(QStringList input_list)
        {
            gender = input_list[1];
            key = input_list[0].right(LETTERS_MAX);
        }
    };

    QList<InputData> reader(QFile * file);
    void trainer(QList<InputData> input);
    //void tester(QFile * file);
};

#endif // GENDERDETECTOR_H
